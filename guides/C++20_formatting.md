# C++20 Formatting library

The 2020 edition of C++ introduces a modern system of converting values into 
human readable strings. This new system is in some ways similar to the old 
printf family of functions with its format string, while being far more safe,
robust, and extensible, it slams face first into the wall of expert-friendly
complexity C++ is infamous for. While it is simpler for the easy cases of
formatting fundamental types and standard strings, adding support for our own
types is not as easy as it should be.

## Simple overview

The formatting library roughly consists of two parts: the formatting functions
and clockwork pieces. For simple uses the formatting functions are all that is
needed, however for supporting our own types we need to delve into the 
confusing collection of clocwork components that keeps the experts happy.

We will ignore some details, overloads, etc. for brevity. The goal of this 
guide is not comprehensive knowledge of the library, that's what 
[cppreference.com](http://en.cppreference.com/w/cpp) is for. The goal here is
to understand how the pieces fit together and why they are as complicated as 
they are.


## Format functions

The primary tool in formatting is the simple `std::format` function, which 
looks a little like this:
```C++
auto std::format(/*static string view*/ format_string, auto&&... format_arguments)
    -> std::string;
```
This works exactly as we'd expect it to:
```C++
assert(std::format("{}{}{}", "hello", 2, 6.4) == "hello26.4");
```
Okay, cool. But what is this _static string view_ type the `format_string` is 
supposed to be? It is essentially a `std::string_view` that refers to a literal
string, fully written in the source code. For example, the following would not 
work:
```C++
std::string myFormatString = "Hello {}!";
std::format(myFormatString, "Name"); // compile error
```
This comes from how the library ensures type safety, and ensures we cannot 
accidentially format a date as a floating point. The natural consequense of
this is that some computation is done during compilation. This magic type is 
named `std::format_string`, however it is not strictly speaking a part of 
C++20, but was added in C++23 with note asking compilers to allow us compiling
in C++20 mode to use it, hence magic.

The other format functions do the same thing, but instead of returning a string
they directly print the results to the given output iterator. These are less 
useful for normal use, but needed for our own type formatting.


## Deep dive down the detail datastream
When calling `std::format` with arguments of, say, `std::string, int, double` 
much like in our example above, a few things happen:
1. The type `std::format_string<std::string, int, double>` is created.
2. The input string is read for the correct amount of replacement fields (`{}`)
3. For each replacement field, an instance of `std::formatter` of the 
appropriate template arguments is created
3. Each replacement field is parsed and anything following a `:` will be passed
to their respective `std::formatter` instance's _parse_ function
4. The formatters are stored
5. Compilation ends
6. Program runs, the function call is reached and the 

In pseudocode it would look something like this:
```C++
auto std::format(inputString, argumentString, argumentInt, argumentDouble)
{
    struct Formatters {
        formatter<string> stringFormatter;
        formatter<int>    intFormatter;
        formatter<double> doubleFormatter;
    };

    Formatters formatters;




}

```

When we want to format our own types, what we need is `std::formatter`, which
for our purposes looks something like this:
```C++
template <typename Type>
struct std::formatter<Type, char> {
    auto parse(/*???*/);
    auto format(const Type& toFormat, /*???*/) const;
};
```


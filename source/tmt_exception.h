#include <concepts>
#include <exception>
#include <format>
#include <source_location>
#include <string>

namespace tmt {

	consteval auto _where(std::source_location _w = std::source_location::current())
		-> std::source_location
	{
		return _w;
	}

	
	class exception : public std::exception {
	public:
		explicit exception(std::string what, std::source_location where = std::source_location::current())
			: m_what{std::move(what)}
			, m_where{where}
		{}

		[[nodiscard]] auto what() const noexcept 
			-> const char* override
		{
			return m_what.c_str();
		}

		[[nodiscard]] virtual auto what_str() const noexcept
			-> const std::string&
		{
			return m_what;
		}

		[[nodiscard]] virtual auto where() const noexcept
			-> const std::source_location&
		{
			return m_where;
		}

		[[nodiscard]] virtual auto where_formatted() const
			-> std::string
		{
			return std::format(
					"{}({},{}): {}",
					m_where.file_name(),
					m_where.line(),
					m_where.column(),
					m_where.function_name()
					);
		}

		[[nodiscard]] virtual auto data_formatted() const
			-> std::string
		{
			return {};
		}

	private:
		std::string m_what;
		std::source_location m_where;
		// stack_trace m_when;
	};

	template <typename T>
	concept _formattable = requires (const T& t) {
		{std::format("{}", t)} -> std::same_as<std::string>;
	};

	template <typename Data>
	class exception_with : public exception {
	public:

		exception_with(std::string what, Data data, std::source_location where = std::source_location::current())
			: exception{std::move(what), where}
			, m_data{std::move(data)}
		{}
		
		[[nodiscard]] auto data_formatted() const
			-> std::string override
		{
			if constexpr (_formattable<Data>) {
				return std::format("{}", m_data);
			}
			else {
				return {};
			}
		}

	private:
		Data m_data;
	};

	namespace _determine_success {
		struct _determine_success_fn {
			constexpr auto determine_success(int value) noexcept
				-> bool
			{
				return value == 0;
			}

			template <typename T>
			[[nodiscard]] constexpr auto operator()(const T& t) const 
				noexcept(noexcept(determine_success(t)))
				-> bool
			{
				return determine_success(t);
			}
		};
	}

	inline namespace _cpos {
		inline constexpr auto determine_success = _determine_success::_determine_success_fn{};
	}

	template <typename T>
	concept success_determinable = std::invocable<_determine_success::_determine_success_fn, T>;

	template <success_determinable ReturnT>
	class requirement_failure : public exception_with<ReturnT> {
	public:

		requirement_failure(std::string what, ReturnT value, std::source_location where = std::source_location::current())
			: exception_with<ReturnT>{std::move(what), std::move(value), std::move(where)}
		{}
	
	private:
	};

	struct _require_success_fn {
		template <success_determinable ReturnT>
		[[nodiscard]] constexpr auto operator()(ReturnT value, std::source_location where = _where()) const
			-> ReturnT
		{
			if (determine_success(value)) {
				return value;
			}
			throw requirement_failure<ReturnT>{"expression required to succeed failed", value, where};
		}
	};

	inline constexpr auto require_success = _require_success_fn{};
}

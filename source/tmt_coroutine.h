#include <compare>
#include <coroutine>
#include <cstddef>
#include <functional>
#include <memory>
#include <type_traits>
#include <utility>

namespace tmt {

	template <typename T>
	concept coroutine_awaiter = requires (T t, std::coroutine_handle<> ch) {
		{t.await_ready()} -> std::convertible_to<bool>;
		{t.await_suspend(ch)};
		{t.await_resume()};
	};

	template <typename T>
	concept coroutine_promise = requires (T t) {
		{t.get_return_object()};
		{t.initial_suspend()} -> coroutine_awaiter;
		{t.final_suspend()} -> coroutine_awaiter;
		{t.unhandled_exception()};
	};
	

	template <typename Promise>
	class unique_coroutine_handle {
	public:

		unique_coroutine_handle() = default;

		unique_coroutine_handle(std::coroutine_handle<Promise> handle) noexcept
			: m_coro{handle}
		{}

		unique_coroutine_handle(unique_coroutine_handle&& other) noexcept
			: m_coro{std::exchange(other.m_coro, nullptr)}
		{}

		auto operator=(unique_coroutine_handle&& other) noexcept
			-> unique_coroutine_handle&
		{
			if (std::addressof(other) != this) {
				m_coro = std::exchange(other.m_coro, nullptr);
			}
			return *this;
		}

		unique_coroutine_handle(const unique_coroutine_handle&) = delete;
		auto operator=(const unique_coroutine_handle&) = delete;

		~unique_coroutine_handle()
		{
			if (m_coro != nullptr) {
				m_coro.destroy();
			}
		}

		[[nodiscard]] operator std::coroutine_handle<>() const noexcept
		{
			return m_coro;
		}

		[[nodiscard]] operator bool() const noexcept
		{
			return m_coro.done();
		}

		[[nodiscard]] auto done() const noexcept
			-> bool
		{
			return m_coro.done();
		}

		[[nodiscard]] auto promise() const noexcept
			-> Promise&
		{
			return m_coro.promise();
		}

		[[nodiscard]] auto address() const noexcept
			-> void*
		{
			return m_coro.address();
		}

		[[nodiscard]] auto handle() const noexcept
			-> std::coroutine_handle<Promise>
		{
			return m_coro;
		}

		void resume() const
		{
			return m_coro.resume();
		}

		void operator()() const
		{
			return m_coro.resume();
		}
		
	private:
		std::coroutine_handle<Promise> m_coro;
	};

	template <typename Promise>
	[[nodiscard]] auto operator==(
			const unique_coroutine_handle<Promise>& left,
			const unique_coroutine_handle<Promise>& right
			)
		noexcept
		-> bool
	{
		return left.handle() == right.handle();
	}

	template <typename Promise>
	[[nodiscard]] auto operator<=>(
			const unique_coroutine_handle<Promise>& left,
			const unique_coroutine_handle<Promise>& right
			)
		noexcept
		-> std::strong_ordering
	{
		return left.handle() <=> right.handle();
	}
}

template <typename Promise>
struct std::hash<tmt::unique_coroutine_handle<Promise>> {
	auto operator()(const tmt::unique_coroutine_handle<Promise>& handle) const noexcept
		-> std::size_t
	{
		return std::hash<std::coroutine_handle<Promise>>{}(handle);
	}
	
};


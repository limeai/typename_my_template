#include <concepts>
#include <cstdint>
#include <initializer_list>
#include <type_traits>

namespace tmt {

	template <typename T>
	concept enum_type = std::is_enum_v<T>;
	
	template <enum_type Enum>
	class flags {
	public:
		constexpr flags() = default;

		constexpr flags(Enum flag) noexcept
			: m_flags{_to_bit(flag)}
		{}

		constexpr void set_flag(Enum flag) noexcept
		{
			m_flags |= _to_bit(flag);
		}

		constexpr void disable_flag(Enum flag) noexcept
		{
			m_flags &= ~_to_bit(flag);
		}

		[[nodiscard]] constexpr auto is_set(Enum flag) const noexcept
			-> bool
		{
			return m_flags & _to_bit(flag);
		}

		template <std::integral Integral>
		[[nodiscard]] constexpr auto as() const noexcept
			-> Integral
		{
			return static_cast<Integral>(m_flags);
		}

		[[nodiscard]] constexpr auto operator==(const flags&) const noexcept -> bool = default;

		
	private:
		constexpr static auto _to_bit(Enum flag)
			-> std::uint64_t
		{
			return 1u << static_cast<std::uint64_t>(flag);
		}

		std::uint64_t m_flags = 0;
	};

	template <enum_type Enum, enum_type... Enums>
	[[nodiscard]] constexpr auto set_flags(Enum first, Enums... rest)
		-> flags<Enum>
	{
		auto result = flags{first};
		if constexpr (sizeof...(rest) > 0) {
			for (const auto flag : {rest...}) {
				result.set_flag(flag);
			}
		}
		return result;
	}


	/*
	 * Here are a bunch of quick tests because I can't be bothered to actually compile this for 
	 * testing. Yay for linters!
	 */
	enum class Test {
		first = 0,
		second,
		third,
	};

	constexpr bool _test_func() 
	{
		auto testy = flags{Test::second};
		testy.set_flag(Test::third);

		if (testy.is_set(Test::first)) {
			return false;
		}

		testy.disable_flag(Test::second);

		return testy == set_flags(Test::third);
	}

	static_assert(set_flags(Test::first, Test::second, Test::third).as<std::uint32_t>() == 0b111);
	static_assert(set_flags(Test::first, Test::third).as<std::uint32_t>() == 0b101);
	static_assert(_test_func());

}
